import pickle
import itertools
import statistics
import json
from datetime import datetime
import requests
import numpy as np
import pandas as pd
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from analysis import smog_weather_data


class PredictionModel(object):

    def __init__(self, columns, degree, model, regression, poly, score):
        self.columns = columns
        self.degree = degree
        self.model = model
        self.regression = regression
        self.poly = poly
        self.score = score

    wind = '566_Prędkość wiatru  [m/s]'
    wind_direction = '566_Kierunek wiatru  [°]'
    temperature = '566_Temperatura powietrza [°C]'
    pressure = '566_Ciśnienie na pozimie stacji [hPa]'
    humid = '566_Wilgotność względna [%]'
    rotated24 = "rotated24"
    rotated8 = "rotated8"
    rotated = "rotated"
    hour = "hour"
    test_data = {
            wind: 2,
            # wind_direction: 300,
            # temperature: 8,
            # pressure: 1024,
            # humid: 70,
            hour: 9
    }



def split_predict(columns, x_train, x_test, y_train, y_test, degree=2):
    poly = PolynomialFeatures(degree=degree)

    x_poly = poly.fit_transform(x_train)
    x_test = poly.fit_transform(x_test)

    clf = linear_model.LinearRegression()
    model = clf.fit(x_poly, y_train)
    score = model.score(x_test, y_test)
    print("Score:", score)
    return PredictionModel(columns, degree, model, clf, poly, score)


def split_selective_predict(columns, joined, indicator):
    print(columns)
    columns_data = joined.loc[:, columns]
    x_train, x_test, y_train, y_test = train_test_split(
            columns_data, joined[indicator], test_size=0.2, random_state=42)

    models = set()
    for degree in range(1, 7):
        models.add(split_predict(columns,
                x_train, x_test, y_train, y_test, degree=degree))
    return models


def get_prediction_data(indicator):
    joined = smog_weather_data(indicator)

    date_time_idx = pd.to_datetime(joined.index, format='%Y-%m-%dT%H:%M:%S')
    joined[PredictionModel.hour] = date_time_idx.strftime('%H')

    columns = list(PredictionModel.test_data.keys())
    for i in range(8, 25, 8):
        column_name = PredictionModel.rotated + str(i)
        joined[column_name] = np.roll(joined[indicator], i)
        columns.append(column_name)

    columns.append(indicator)

    joined = joined.loc[:, columns]
    joined = joined.dropna()
    return joined


def test_split(indicator="MpKrakAlKras-PM2.5-1g"):
    joined = get_prediction_data(indicator)

    ret = set()
    model_columns = set(joined.columns)
    model_columns.remove(indicator)
    for size in range(2, len(model_columns) + 1):
        cols_combinations = itertools.combinations(model_columns, size)
        for cols in cols_combinations:
            models = split_selective_predict(cols, joined, indicator)
            ret.update(models)
    return ret


def save_obj(obj, name):
    with open(name, 'wb') as out:
        pickle.dump(obj, out, pickle.HIGHEST_PROTOCOL)


def load_obj(name):
    with open(name, 'rb') as in_file:
        return pickle.load(in_file)


def main():
    indicator = "MpKrakAlKras-PM2.5-1g"
    models_file = "predict_models.pkl"

    try:
        models = load_obj(models_file)
    except FileNotFoundError as err:
        print(err)
        models = test_split(indicator)
        save_obj(models, models_file)

    all_data = get_prediction_data(indicator)
    preds_list = list()
    data_8h = data_for_8h()
    for model in list(models):
        if not set(model.columns).issubset(set(data_8h.keys())):
            continue

        joined = all_data.loc[:, model.columns + (indicator, )]
        joined = joined.dropna()

        poly = model.poly
        data = joined.loc[:, model.columns]
        data = poly.transform(data)

        predict = [[data_8h[column] for column in model.columns]]
        predict = poly.transform(predict)

        scr = model.model.score(data, joined[indicator])
        pred = model.model.predict(predict)
        print(model.columns, model.degree)
        print("Score:", scr, "Predict: ", pred)
        preds_list.append(pred)
    print("median", statistics.median(preds_list))


def get_airly_measurment(data, name="PM25", hours_back=23):
    values = data["history"][23 - hours_back]["values"]
    for value in values:
        if value["name"] == name:
            return value["value"]
    return None


def get_weather_forecast():
    data = requests.get(
            "https://api.openweathermap.org/data/2.5/forecast"
            "?q=Krakow,pl&units=metric&APPID=87f92398d2f458bec975b2eb52d68410")
    parsed = json.loads(data.text)
    weather = parsed["list"][2]
    return weather


def get_airly_measurement():
    session = requests.Session()
    session.headers.update({'Accept': 'application/json',
                            'apikey': '9uVMqE9anf0CsMC2NHt8l0zgTfxOePkM'})
    resp = session.get('https://airapi.airly.eu/v2/measurements/'
            'point?lat=50.062006&lng=19.940984')
    airly = json.loads(resp.text)
    return airly


def data_for_8h():
    weather = get_weather_forecast()
    airly = get_airly_measurement()

    hour_ = datetime.strptime(airly["current"]["fromDateTime"],
            '%Y-%m-%dT%H:%M:%S.%fZ').hour
    ret = {
        PredictionModel.wind: weather["wind"]["speed"],
        PredictionModel.wind_direction: weather["wind"]["deg"],
        PredictionModel.temperature: weather["main"]["temp"],
        # PredictionModel.pressure: weather["main"]["pressure"],
        PredictionModel.humid: weather["main"]["humidity"],
        PredictionModel.rotated8: get_airly_measurment(airly, hours_back=0),
        PredictionModel.rotated24: get_airly_measurment(airly, hours_back=16),
        PredictionModel.hour: hour_
    }
    return ret


if __name__ == "__main__":
    main()
