import zipfile
import os
import requests
from bs4 import BeautifulSoup
from imgw import DATA_DIR
from imgw import FORMAT_FILE


BASE_URL = "https://dane.imgw.pl/data/dane_pomiarowo_obserwacyjne/"\
        "dane_meteorologiczne/terminowe/synop/"
URL = BASE_URL + "2017/"


def get_links(url):
    resp = requests.get(url)
    data = resp.text
    soup = BeautifulSoup(data, features="html.parser")

    ret = list()
    for link in soup.find_all('a'):
        href = link.get('href')
        if href.endswith("zip"):
            ret.append(link.get('href'))

    return ret


def download_file(base_url, file_name):
    url = base_url + file_name
    resp = requests.get(url)
    with open(os.path.join(DATA_DIR, file_name), "wb") as out:
        out.write(resp.content)
    print("Downloaded: ", url)


def unzip_file(file_name):
    zip_ref = zipfile.ZipFile(os.path.join(DATA_DIR, file_name), 'r')
    zip_ref.extractall(DATA_DIR)
    zip_ref.close()


def get_files(url):
    if not os.path.exists(DATA_DIR):
        os.mkdir(DATA_DIR)

    download_file(BASE_URL, FORMAT_FILE)
    for link in get_links(url):
        download_file(URL, link)
        unzip_file(link)


def main():
    get_files(URL)


if __name__ == "__main__":
    main()
