import os
import pandas as pd
from gios import GIOS_DIR
from airly import AIRLY_DIR
from airly import get_data_for_sensor


class SensorLocator(object):

    def __init__(self, sensors_file=os.path.join(
            AIRLY_DIR, "sensor_locations.csv")):
        self.sensors_file = sensors_file
        self.sensors = list()
        locations_file = os.path.join(AIRLY_DIR, "sensor_locations.csv")
        with open(locations_file) as locations:
            for location in locations:
                self.sensors.append(location.split(","))
        self.sensors = self.sensors[1:]

    @staticmethod
    def _metric(lon, lat, sensor):
        sensor_lon = float(sensor[1])
        sensor_lat = float(sensor[2])
        dist = ((lon - sensor_lon)**2 + (lat - sensor_lat)**2)
        return dist

    def _select_by_coords(self, lon, lat):
        sensor = min(self.sensors,
                key=lambda s: (self._metric(lon, lat, s)))
        return sensor

    @staticmethod
    def _select_for_station(stations_file, station_name, column):
        stations_meta = pd.read_excel(stations_file)
        station_row = stations_meta[
                'Kod stacji'].eq(station_name).idxmax()
        return float(stations_meta.at[station_row, column])

    def select_for_station(self, station="MpKrakAlKras",
            stations_file=os.path.join(GIOS_DIR, "Metadane_wer20180829.xlsx")):

        lon = self._select_for_station(stations_file, station, 'WGS84 φ N')
        lat = self._select_for_station(stations_file, station, 'WGS84 λ E')

        print("Station: ", station, lon, lat)
        sensor = self._select_by_coords(lon, lat)
        print("Sensor: ", sensor)
        return sensor

    def get_data_for_station(self, station_name):
        sensor = self.select_for_station(station_name)
        sensor_id = sensor[0]

        return get_data_for_sensor(sensor_id)


def main():
    locator = SensorLocator()
    sensor = locator.select_for_station()
    print(sensor)


if __name__ == "__main__":
    main()
