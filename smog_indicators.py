from gios import GiosData
from analysis import save_heatmap
from analysis import save_pairplot


def main():
    gios = GiosData().get_krk_data()

#    stations = ["MpKrakAlKras", "MpKrakBujaka",
#            "MpKrakBulwar", "MpKrakDietla"]
    stations = ["MpKrakAlKras"]
    for station in stations:
        station_cols = [c for c in gios.columns if station in c]
        columns = ['UTC'] + station_cols

        data_subset = gios.loc[:, columns]

        save_heatmap(data_subset, station + '_heatmap_smog.png')
        save_pairplot(data_subset, station + "_pairplot_smog.png")

        describe = data_subset.describe()
        print(describe)


if __name__ == "__main__":
    main()
