import os
import glob
import pandas as pd


GIOS_DIR = "gios"
DATA_1H = os.path.join(GIOS_DIR, "*_1g.xlsx")
ALL_KRK_DATA = os.path.join(GIOS_DIR, "all_krk_data.csv")
ALL_1H_DATA = os.path.join(GIOS_DIR, "all_1h_data.csv")


class GiosData(object):

    def get_krk_data(self):
        if not os.path.exists(ALL_KRK_DATA):
            self.save_all_to_csv()
        return pd.read_csv(ALL_KRK_DATA)

    def save_all_to_csv(self):
        frames = self.get_krk_frames()
        all_data = self.join_frames(frames)
        all_data.to_csv(ALL_KRK_DATA, encoding='utf-8')

    @staticmethod
    def join_frames(frames):
        joined = frames[0]
        joined = joined.set_index("UTC")
        for frame in frames[1:]:
            frame = frame.set_index("UTC")
            joined = joined.join(frame)
        return joined

    def get_krk_frames(self):
        frames = list()
        for file_name in glob.glob(DATA_1H):
            data_xls = self._get_krk_data(file_name)
            frames.append(data_xls)
        return frames

    @staticmethod
    def get_all_1h_data():
        pds = list()
        for exel in glob.glob(DATA_1H):
            print(exel)
            data_xls = pd.read_excel(exel)
            columns = data_xls.iloc[4]
            columns = ["UTC", *columns[1:]]
            data_xls.columns = columns
            data_xls = data_xls.iloc[5:]
            data_xls["UTC"] = data_xls["UTC"].transform(
                    (lambda time_str: str(time_str).replace(" ", "T")))
            pds.append(data_xls)
        return GiosData.join_frames(pds)

    @staticmethod
    def _get_krk_data(file_name):
        print(file_name)
        data_xls = pd.read_excel(file_name)

        columns = data_xls.iloc[4]
        columns = ["UTC", *columns[1:]]
        data_xls.columns = columns
        data_xls = data_xls.iloc[5:]

        select_columns = [c for c in columns if "MpKrak" in c]
        if not select_columns:
            print("No data for krk")
            return pd.DataFrame({'UTC': []})

        for column in select_columns:
            data_xls[column] = data_xls[column].transform(GiosData.to_float)

        data_xls = data_xls[["UTC", *select_columns]]

        data_xls["UTC"] = data_xls["UTC"].transform(
                (lambda time_str: str(time_str).replace(" ", "T")))
        return data_xls

    @staticmethod
    def to_float(data):
        if isinstance(data, float):
            return data
        return float(data.replace(",", "."))


def get_gios_krk_cvs_data():
    data_file = os.path.join(GIOS_DIR, "merged.csv")
    if not os.path.exists(data_file):
        save_merged_csv()
    return pd.read_csv(data_file)


def save_merged_csv():
    gios = GiosData()
    krk_data = gios.get_krk_data()
    print(krk_data.columns)

    selected_columns = ['UTC',
                        'MpKrakAlKras-C6H6-1g',
                        'MpKrakAlKras-CO-1g',
                        'MpKrakAlKras-NO2-1g',
                        'MpKrakAlKras-NOx-1g',
                        'MpKrakBujaka-O3-1g',
                        'MpKrakAlKras-PM10-1g',
                        'MpKrakAlKras-PM2.5-1g',
                        'MpKrakBujaka-SO2-1g',
                        'MpKrakBulwar-SO2-1g']
    krk_data = krk_data.loc[:, selected_columns]
    krk_data.to_csv(os.path.join(GIOS_DIR, "merged.csv"),
            encoding='utf-8')


def main():
    gios = GiosData()
    krk_data = gios.get_krk_data()
    print(krk_data)


def save_all_1h_data():
    data = GiosData.get_all_1h_data()
    data.to_csv(ALL_1H_DATA, encoding='utf-8')
    print(data)


if __name__ == "__main__":
    main()
