
## This is source code for simple data analisys project, targeting smog in Krak�w

Below is list of data sources, analisys will be avaliable in [Wiki](https://bitbucket.org/karulis/krk_clean_air/wiki/Home).

* [Airly](https://airly.eu/) sensors, archived on [kaggle](https://www.kaggle.com/datascienceairly/air-quality-data-from-extensive-network-of-sensors)(manual download only)
* [IMGW](http://www.imgw.pl/)(note the lack of https) archived [data](https://dane.imgw.pl/data/dane_pomiarowo_obserwacyjne/dane_meteorologiczne/terminowe/synop/2017/)(downloaded by script in repository)
* [GIOS](http://www.gios.gov.pl/en/)(note the lack of https) archived [data](https://powietrze.gios.gov.pl/pjp/archives)(needs manual download)
