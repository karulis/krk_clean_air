import os
import pickle
import json
import logging
from datetime import datetime
from collections import defaultdict
from concurrent import futures
from statistics import median
import requests
from bokeh.io import show, output_file
from bokeh.plotting import figure


PREDS_DIR = "predictions"


class PredictionModel(object):

    wind = '566_Prędkość wiatru  [m/s]'
    wind_direction = '566_Kierunek wiatru  [°]'
    temperature = '566_Temperatura powietrza [°C]'
    pressure = '566_Ciśnienie na pozimie stacji [hPa]'
    humid = '566_Wilgotność względna [%]'
    rotated24 = "rotated24"
    rotated16 = "rotated16"
    rotated8 = "rotated8"
    rotated = "rotated"
    hour = "hour"


def load_obj(name):
    with open(name, 'rb') as in_file:
        return pickle.load(in_file)


def get_weather_forecast():
    data = requests.get(
            "https://api.openweathermap.org/data/2.5/forecast"
            "?q=Krakow,pl&units=metric&APPID=87f92398d2f458bec975b2eb52d68410")
    return json.loads(data.text)


def gios_measurement():
    data = requests.get(
            "https://api.gios.gov.pl/pjp-api/rest/data/getData/2752")
    parsed = json.loads(data.text)
    return parsed["values"]


def gios_back_measurment(data, hours_back):
    value = data[hours_back]["value"]
    if value is None:
        logging.debug("Got none value for hour %s in gios %s",
                hours_back, data[hours_back])
        value = data[hours_back + 1]["value"]
    logging.debug("gios_back_measurment %s", value)
    return value


def airly_measurement():
    session = requests.Session()
    session.headers.update({'Accept': 'application/json',
                            'apikey': '9uVMqE9anf0CsMC2NHt8l0zgTfxOePkM'})
    resp = session.get('https://airapi.airly.eu/v2/measurements/'
            'point?lat=50.062006&lng=19.940984')
    airly = json.loads(resp.text)
    return airly


def airly_back_measurment(data, name="PM25", hours_back=23):
    history = data["history"]
    hour = history[23 - hours_back]
    print(history[23 - hours_back]["fromDateTime"])
    values = hour["values"]
    for value in values:
        if value["name"] == name:
            return value["value"]
    return None


def weather_forcast_hour(weather, hour):
    return weather["list"][int(hour / 3)]


def prediction_for_hour(hours, weather, airly, gios):

    now = datetime.now()
    hour_ = (now.hour + hours) % 24

    weather = weather_forcast_hour(weather, hours)
    ret = {
        PredictionModel.wind: weather["wind"]["speed"],
        PredictionModel.wind_direction: weather["wind"]["deg"],
        PredictionModel.temperature: weather["main"]["temp"],
        PredictionModel.pressure: weather["main"]["grnd_level"],
        PredictionModel.humid: weather["main"]["humidity"],
        PredictionModel.hour: hour_
    }

    hours_back = (8 - hours)
    if hours_back >= 0:
        ret[PredictionModel.rotated8] = gios_back_measurment(gios, hours_back)
    hours_back = (16 - hours)
    if hours_back >= 0:
        ret[PredictionModel.rotated16] = gios_back_measurment(gios, hours_back)
    hours_back = (24 - hours)
    if hours_back >= 0:
        ret[PredictionModel.rotated24] = gios_back_measurment(gios, hours_back)

    return ret


def model_predictions(utc, model, weather, airly, gios):
    predictions = dict()

    hours = list(range(1, 25))
    for hour in hours:
        data = prediction_for_hour(hour, weather, airly, gios)
        if not set(model.columns).issubset(set(data.keys())):
            logging.debug("Problem  with: %s %s", model.columns,
                    set(model.columns) - set(data.keys()))
            predictions[hour] = "NaN"
            continue

        predict = [[data[column] for column in model.columns]]
        predict = model.poly.transform(predict)

        pred = model.model.predict(predict)
        predictions[hour] = pred[0]

    file_name = "{COLS}-{DEG}-{SCR}.log"
    file_name = file_name.format(COLS="-".join(model.columns),
            DEG=model.degree, SCR=model.score)
    file_name = file_name.replace("/", "")
    file_name = os.path.join(PREDS_DIR, file_name)
    with open(file_name, "a+") as out:
        out.write(str(utc))
        for hour in hours:
            out.write(" " + str(predictions[hour]))
        out.write("\n")

    return predictions


def dump_data(utc, weather, airly, gios):
    # data = {"weather": weather, "airly": airly, "gios": gios}
    file_name = os.path.join(PREDS_DIR, "measurments.data")
    hours = list(range(1, 25))
    with open(file_name, "a+") as out:
        out.write(str(utc))
        for hour in hours:
            data = prediction_for_hour(hour, weather, airly, gios)
            out.write("|" + str(data))
        out.write("\n")


def main():
    models = load_obj("predict_models.pkl")
    weather = get_weather_forecast()
    airly = airly_measurement()
    gios = gios_measurement()
    utc = datetime.utcnow()
    dump_data(utc, weather, airly, gios)

    futures_list = list()
    with futures.ProcessPoolExecutor() as executor:
        for model in list(models):
            # if model.score < 0.75:
            #     continue

            future = executor.submit(model_predictions,
                    utc, model, weather, airly, gios)
            futures_list.append(future)

    predictions = defaultdict(list)
    for future in futures_list:
        for hour, estimate in future.result().items():
            if estimate == "NaN":
                continue
            predictions[hour].append(estimate)
    show_plot(predictions)


def show_plot(predictions):
    output_file("smog_predictions.html")
    logging.debug(predictions)

    hours = predictions.keys()
    medians = [median(predictions[h]) for h in hours if predictions[h]]
    hours = [str(h) for h in hours]

    fig = figure(x_range=hours, plot_height=350, title="PM 2.5 predictions",
            toolbar_location=None, tools="")

    fig.vbar(x=hours, top=medians, width=0.9)

    fig.xgrid.grid_line_color = None
    fig.y_range.start = 0

    show(fig)


if __name__ == "__main__":
    main()
