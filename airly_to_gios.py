import os
import seaborn as sns
from airly import get_data_for_sensor
from gios import GiosData
from analysis import DATA_DIR
from analysis import PLOTS_DIR


def main():
    for directory in [DATA_DIR, PLOTS_DIR]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    sensor_id = "189"
    column_names = [
            sensor_id + "_pm1",
            sensor_id + "_pm25",
            sensor_id + "_pm10",
            "MpKrakAlKras-PM10-1g",
            "MpKrakAlKras-PM2.5-1g"]

    sensors_data = get_data_for_sensor(sensor_id)
    gios = GiosData().get_krk_data()

    all_data = sensors_data.set_index("UTC").join(gios.set_index("UTC"))

    data_subset = all_data.loc[:, column_names]
    data_subset.to_csv(os.path.join(DATA_DIR, "airly_gios.csv"),
            encoding='utf-8')

    corr = data_subset.corr()
    heat = sns.heatmap(corr, annot=True)
    figure = heat.get_figure()
    figure.savefig(os.path.join(PLOTS_DIR, 'heatmap_airly_gios.png'),
            bbox_inches="tight")

    pairplot = sns.pairplot(data_subset)
    pairplot.savefig(os.path.join(PLOTS_DIR, "pairplot_airly_gios.png"))

    data_subset['PM2.5 diff'] = data_subset["MpKrakAlKras-PM2.5-1g"]\
            - data_subset[sensor_id + "_pm25"]
    data_subset['PM10 diff'] = data_subset["MpKrakAlKras-PM10-1g"]\
            - data_subset[sensor_id + "_pm10"]
    print(data_subset.describe())
    print(data_subset.median())


if __name__ == "__main__":
    main()
