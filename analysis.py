import os
import numpy
import seaborn as sns
import matplotlib.pyplot as plt
from airly import get_data_for_sensor
from imgw import get_meteo_data
from imgw import get_column_names
from gios import get_gios_krk_cvs_data
from gios import GiosData


DATA_DIR = "data"
PLOTS_DIR = "plots"


def get_columns():
    return [col for col in get_column_names()[6:] if "Status" not in col]


def smog_weather_data(indicator):
    gios_data = GiosData().get_krk_data()
    gios_data = gios_data.loc[:, [indicator, "UTC"]]

    imgw_data = get_meteo_data(relevant_columns=get_columns())

    joined = gios_data.set_index("UTC").join(imgw_data.set_index("UTC"))
    joined = joined.dropna(axis='columns', how='all')
    joined = joined.sort_index()
    return joined


def cleanup_plot():
    plt.close()
    plt.clf()
    plt.cla()


def save_lineplot(x_var, y_var, data, file_name):
    sns.lineplot(x=x_var, y=y_var, data=data, estimator=numpy.median)
    plot_name = os.path.join(PLOTS_DIR, file_name)
    plt.savefig(plot_name)
    cleanup_plot()


def save_barplot(x_var, y_var, data, file_name):
    sns.barplot(x=x_var, y=y_var, data=data)
    plot_name = os.path.join(PLOTS_DIR, file_name)
    plt.savefig(plot_name)
    cleanup_plot()


def save_jointplot(x_var, y_var, file_name):
    with sns.axes_style("white"):
        jointplot = sns.jointplot(x=x_var, y=y_var, kind="hex", color="k")
        jointplot.savefig(os.path.join(PLOTS_DIR, file_name),
                bbox_inches="tight")
        cleanup_plot()


def save_distplot(data, file_name):
    distplot = sns.distplot(data)
    distplot.savefig(os.path.join(PLOTS_DIR, file_name), bbox_inches="tight")
    cleanup_plot()


def save_heatmap(data, file_name):
    corr = data.corr()
    heat = sns.heatmap(corr, annot=True)
    figure = heat.get_figure()
    figure.savefig(os.path.join(PLOTS_DIR, file_name), bbox_inches="tight")
    cleanup_plot()


def save_pairplot(data, file_name):
    pairplot = sns.pairplot(data)
    pairplot.savefig(os.path.join(PLOTS_DIR, file_name))
    cleanup_plot()


def save_regplot(data, xvar, yvar, file_name="regplot.png", order=2):
    data = data.loc[:, [
            xvar,
            yvar,
    ]]
    data = data.dropna()

    ret = numpy.polyfit(data[xvar], data[yvar], deg=order)
    reg = sns.regplot(x=xvar, y=yvar, data=data, color='b', order=order,
            line_kws={'label': "coords: {}".format(ret)})
    reg.legend()
    reg.set_title("Polynomial order: " + str(order))
    figure = reg.get_figure()
    figure.savefig(os.path.join(PLOTS_DIR, file_name), bbox_inches="tight")
    cleanup_plot()
    return ret


def main():
    imgw_data = get_meteo_data()
    imgw_data.to_csv(os.path.join(DATA_DIR, "imgw_data.csv"), encoding='utf-8')

    balice_closest_sensor = "218"
    sensors_data = get_data_for_sensor(balice_closest_sensor)
    sensors_data.to_csv(os.path.join(DATA_DIR, "sensors_data.csv"),
            encoding='utf-8')

    gios = get_gios_krk_cvs_data()
    gios.to_csv(os.path.join(DATA_DIR, "gios.csv"), encoding='utf-8')

    joined = imgw_data.set_index("UTC").join(sensors_data.set_index("UTC"))
    joined = joined.join(gios.set_index("UTC"))

#    joined = joined.sort_index().loc[
#            '2017-01-01T01:00:00':'2017-01-03T00:00:00']
    joined.to_csv(os.path.join(DATA_DIR, "all.csv"), encoding='utf-8')

    corr = joined.corr()
    heat = sns.heatmap(corr)
    figure = heat.get_figure()
    figure.savefig(os.path.join(PLOTS_DIR, 'heatmap.png'), bbox_inches="tight")

    pairplot = sns.pairplot(joined)
    pairplot.savefig(os.path.join(PLOTS_DIR, "pairplot.png"))


if __name__ == "__main__":
    main()
