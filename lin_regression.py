from analysis import save_regplot
from gios import GiosData


def regresion(station="MpKrakBujaka"):
    data_subset = GiosData().get_krk_data()

    data_subset["PM10"] = data_subset[station + "-PM10-1g"]
    data_subset["PM25"] = data_subset[station + "-PM2.5-1g"]
    data_subset["C6H6"] = data_subset[station + "-C6H6-1g"]
    data_subset["SO2"] = data_subset[station + "-SO2-1g"]
    data_subset["O3"] = data_subset[station + "-O3-1g"]

    save_regplot(data_subset, 'PM25', 'PM10', "linreg_pm25_pm10")
#    save_regplot(data_subset, 'PM25', 'SO2', "linreg_pm25_so2")
#    save_regplot(data_subset, 'PM25', 'C6H6', "linreg_pm25_c6h6")

    data_pm25 = data_subset['PM25']
    data_pm10 = data_subset['PM10']
#    print(data_subset[(data_pm25 > 30) & (data_pm10 > 50)])
#    print(data_subset[(data_pm25 <= 30) & (data_pm10 <= 50)])
#    print(data_subset[(data_pm25 > 30) & (data_pm10 <= 50)])
    print(data_subset[(data_pm25 <= 30) & (data_pm10 > 50)])
#    print(data_subset[(data_subset['PMC6H6'] > 5)])
#    print(data_subset['PMC6H6'].mean())


if __name__ == "__main__":
    regresion()
