from analysis import save_regplot
from analysis import save_jointplot
from analysis import save_lineplot
from analysis import smog_weather_data


def get_data(indicator):
    joined = smog_weather_data(indicator)
    joined = joined[('2017-04-01T00:00:00' > joined.index)
            (joined.index > '2017-10-01T00:00:00')]
    return joined


def calc_corr(indicator):
    joined = get_data(indicator)

    print_corr(joined, 'pearson', indicator)
    print_corr(joined, 'spearman', indicator)
    print_corr(joined, 'kendall', indicator)
    generate_plots(joined, indicator)


def save_plots(data, indicator, column):
    plot_name = column + "-" + indicator.split('-', 1)[1]
    plot_name = plot_name.replace("/", "").replace(".", "")
    median_data = data.groupby(column)[[indicator]].median()
    median_data = median_data.reset_index()
    try:
        for i in range(1, 4):
            save_regplot(median_data, column, indicator,
                    "reg_" + plot_name + str(i), order=i)
        save_jointplot(data[column], data[indicator], "join_" + plot_name)
        save_lineplot(column, indicator, data, "lineplot_" + plot_name)
    except Exception as err:
        print(err, plot_name)


def _generate_plots(joined, indicator, column):
    data = joined.loc[:, [indicator, column]]
    data = data.dropna()
    dtype = data[column].dtype
    if dtype == 'object':
        print("Not numeric", dtype)
        return
    if data[column].isnull().all():
        print("All null", column)
        return
    if all(data[column] == 0):
        print("All zero", column)
        return
    save_plots(joined, indicator, column)


def generate_plots(joined, indicator):

    for column in joined.columns:
        if not column == indicator:
            _generate_plots(joined, indicator, column)


def main():
    calc_corr("MpKrakAlKras-PM2.5-1g")
    # calc_corr("MpKrakAlKras-PM10-1g")
    # calc_corr("MpKrakAlKras-C6H6-1g")
    # calc_corr("MpKrakAlKras-NO2-1g")
    # calc_corr("MpKrakAlKras-NOx-1g")
    # calc_corr("MpKrakBujaka-SO2-1g")
    # calc_corr("MpKrakBujaka-O3-1g")
    # calc_corr("MpKrakAlKras-CO-1g")


def print_corr(data, method, indicator, edge=0.5):
    print(method, indicator)
    corr = data.corr(method=method)
    corr = corr.loc[:, [indicator]]
    corr = corr[(-edge > corr[indicator]) | (corr[indicator] > edge)]
    print(corr)


if __name__ == "__main__":
    main()
