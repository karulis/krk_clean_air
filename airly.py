import glob
import os
import pandas as pd


AIRLY_DIR = "airly"
UTC_COL = "UTC"


def get_sensors_data():
    pds = list()
    for csv in glob.glob(os.path.join('airly', "*2017.csv")):
        pds.append(pd.read_csv(csv))

    sensors_data = pd.concat(pds, sort=True)
    sensors_data = sensors_data.rename(columns={'UTC time': UTC_COL})
    return sensors_data


def get_data_for_sensor(sensor_id):
    sensors_data = get_sensors_data()

    columns = ["temperature", "humidity", "pressure", "pm1", "pm25", "pm10"]
    prefix = sensor_id + "_"
    prefix_columns = [UTC_COL]
    prefix_columns += [(prefix + c) for c in columns]
    sensors_data = sensors_data.loc[:, prefix_columns]
    sensors_data = sensors_data.dropna()

    return sensors_data.sort_values(by=UTC_COL)


def main():
    balice_closest_sensor = "218"
    sensors_data = get_data_for_sensor(balice_closest_sensor)
    sensors_data.to_csv(os.path.join(AIRLY_DIR, "sensors.data"), index=False)


def save_all():
    data = get_sensors_data()
    data = data.set_index(UTC_COL)
    data.sort_values(by=UTC_COL)
    data.to_csv(os.path.join(AIRLY_DIR, "all_sensors.data"))
    print(data)


if __name__ == "__main__":
    main()
