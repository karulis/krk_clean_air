import glob
import os
import pandas as pd


DATA_DIR = "imgw"
FORMAT_FILE = "s_t_format.txt"
ALL_IMGW_DATA = os.path.join(DATA_DIR, "all_imgw.csv")


def row_to_utc(row):
    str_row = row.astype(str)
    ret = str_row[0]
    ret += '-{:02d}'.format(int(str_row[1]))
    ret += '-{:02d}'.format(int(str_row[2]))
    ret += "T{H:02d}:00:00".format(H=int(str_row[-1]))
    return ret


def get_column_names():
    columns = list()
    with open(os.path.join(DATA_DIR, FORMAT_FILE), encoding="ISO-8859-1")\
            as format_data:
        for line in format_data.readlines()[:-3]:
            line = line.strip()
            if line:
                column_name, _ = line.rsplit(" ", 1)
                columns.append(column_name.strip())
    return columns


def get_meteo_data(data_file=os.path.join(DATA_DIR, 's_t_566_2017.csv'),
        relevant_columns=["Prędkość wiatru  [m/s]",
                          "Kierunek wiatru  [°]",
                          "Poryw wiatru  [m/s]",
                          "Wilgotność względna [%]",
                          "Opad za 6 godzin [mm]",
                          "Poryw maksymalny za okres WW [m/s]"]):
    column_names = get_column_names()
    meteo_data = pd.read_csv(data_file, names=column_names,
            encoding="ISO-8859-1")

    time_data = meteo_data.loc[:, column_names[2:6]]
    relevant_data = meteo_data.loc[:, relevant_columns]
#    relevant_data = meteo_data.loc[:, column_names[6:]]
    prefix = data_file.split("_")[-2] + "_"
    relevant_data = relevant_data.add_prefix(prefix)
    relevant_data["UTC"] = time_data.apply(row_to_utc, axis=1)
    relevant_data.to_csv("tmp.csv")
    return relevant_data


def get_all_data():
    pds = list()
    for csv in glob.glob(os.path.join(DATA_DIR, "*2017.csv")):
        print(csv)
        pds.append(get_meteo_data(csv))

    return join_frames(pds)


def main():
    print(get_meteo_data())


def join_frames(frames):
    joined = frames[0]
    joined = joined.set_index("UTC")
    for frame in frames[1:]:
        frame = frame.set_index("UTC")
        joined = pd.merge(joined, frame, on="UTC")
    return joined


def save_all_data():
    data = get_all_data()
    data.to_csv(ALL_IMGW_DATA, encoding='utf-8')


if __name__ == "__main__":
    main()
