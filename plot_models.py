import os
import glob
import math
import pandas as pd
from bokeh.plotting import figure, show, output_file


PREDS_DIR = "predictions"


def get_predictions():
    rows_list = list()

    for data_file in glob.glob(os.path.join(PREDS_DIR, "*.log")):
        if "--" in data_file:
            # print("error ", data_file)
            continue

        score = float(data_file.rpartition("-")[-1].rpartition(".")[0])
        if not ("rotated24" in data_file and score > 0.55):
            continue
        # print(data_file)
        with open(data_file) as data_in:
            data = data_in.readlines()[-1]
            data = data.split()[2:]
            data = [float(p) for p in data]
            if not all(0.0 < p < 1000.0 for p in data):
                continue

            for idx, prediction in enumerate(data, start=1):
                row = dict()
                row["hour"] = idx
                row["prediction"] = prediction
                # row["model"] = "model_name"
                rows_list.append(row)
    print("number of models: ", len(rows_list))
    return pd.DataFrame(rows_list)


def main():
    predictions = get_predictions()

    p1 = figure(sizing_mode='stretch_both',
            title="Years vs mpg without jittering")
    p1.xaxis[0].ticker = sorted(predictions.hour.unique())
    p1.circle(x='hour', y='prediction', size=9, alpha=0.4, source=predictions)

    output_file("jitter.html")

    show(p1)


def box_plot():
    # generate some synthetic time series for six different categories
    predictions = get_predictions()
    df = predictions

    # find the quartiles and IQR for each category
    groups = df.groupby('hour')
    q1 = groups.quantile(q=0.25)
    q2 = groups.quantile(q=0.5)
    q3 = groups.quantile(q=0.75)
    iqr = q3 - q1
    upper = q3 + 1.5*iqr
    lower = q1 - 1.5*iqr

    # find the outliers for each category
    def outliers(group):
        cat = group.name
        return group[(group.prediction > upper.loc[cat]['prediction'])
                | (group.prediction < lower.loc[cat]['prediction'])]['prediction']
    out = groups.apply(outliers).dropna()

    # prepare outlier data for plotting, we need coordinates for every outlier.
    if not out.empty:
        outx = []
        outy = []
        for keys in out.index:
            outx.append(keys[0] - 0.5)
            outy.append(out.loc[keys[0]].loc[keys[1]])

    cats = sorted(set(predictions.hour))
    cats = [str(c) for c in cats]

    p = figure(tools="", background_fill_color="#efefef", x_range=cats,
            sizing_mode='stretch_both')

    # if no outliers, shrink lengths of stems to be no longer than the minimums or maximums
    qmin = groups.quantile(q=0.00)
    qmax = groups.quantile(q=1.00)
    upper.prediction = [min([x,y]) for (x,y) in zip(list(qmax.loc[:,'prediction']),upper.prediction)]
    lower.prediction = [max([x,y]) for (x,y) in zip(list(qmin.loc[:,'prediction']),lower.prediction)]

    # stems
    p.segment(cats, upper.prediction, cats, q3.prediction, line_color="black")
    p.segment(cats, lower.prediction, cats, q1.prediction, line_color="black")

    # boxes
    p.vbar(cats, 0.7, q2.prediction, q3.prediction, fill_color="#E08E79", line_color="black")
    p.vbar(cats, 0.7, q1.prediction, q2.prediction, fill_color="#3B8686", line_color="black")

    # whiskers (almost-0 height rects simpler than segments)
    p.rect(cats, lower.prediction, 0.2, 0.01, line_color="black")
    p.rect(cats, upper.prediction, 0.2, 0.01, line_color="black")

    # outliers
    if not out.empty:
        p.circle(outx, outy, size=6, color="#F38630", fill_alpha=0.6)

    p.xgrid.grid_line_color = None
    p.ygrid.grid_line_color = "white"
    p.grid.grid_line_width = 2
    p.xaxis.major_label_text_font_size="12pt"

    output_file("boxplot.html", title="boxplot.py example")

    show(p)


if __name__ == "__main__":
    main()
    box_plot()
