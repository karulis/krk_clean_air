import os
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from gios import GiosData
from analysis import PLOTS_DIR


def get_color(idx, deck_size):
    deck_min = min(deck_size)
    deck_max = max(deck_size)
    diff = deck_max - deck_min
    idx = (deck_max - idx) / diff
    idx = int(15 * idx)
    # 0x00ff00 -> 0xffff00 -> 0xff0000
    val = 0xa00000 + idx * 0x011001
    ret = "#{:06x}".format(val)
    return ret


def month_bar_plot(indicator="MpKrakAlKras-PM2.5-1g", name="PM25", norm=0):
    data_subset = GiosData().get_krk_data()

    data_subset["UTC"] = pd.to_datetime(data_subset["UTC"],
            format='%Y-%m-%dT%H:%M:%S')
    data_subset = data_subset.set_index("UTC")
    data_subset = data_subset.resample('M').mean()

    data_subset = data_subset.reset_index()
    data_subset['month'] = data_subset['UTC'].astype(str).str[5:7]

    data_subset[name] = data_subset[indicator]

    values = data_subset[name]
    palette = [get_color(d, values) for d in values]

    sns.barplot(x="month", y=name, data=data_subset, palette=palette)
    if norm:
        plt.axhline(color='r', y=norm)

    plot_name = os.path.join(PLOTS_DIR, name + "_bar_plot.png")
    plt.savefig(plot_name)


if __name__ == "__main__":
    # month_bar_plot("MpKrakAlKras-PM2.5-1g", "PM25", 10)
    # month_bar_plot("MpKrakAlKras-PM10-1g", "PM10", 15)
    # month_bar_plot("MpKrakAlKras-C6H6-1g", "C6H6", 5)
    # month_bar_plot("MpKrakAlKras-NO2-1g", "NO2", 40)
    # month_bar_plot("MpKrakBujaka-NOx-1g", "NOx")
    # month_bar_plot("MpKrakBujaka-SO2-1g", "SO2", 20)
    # month_bar_plot("MpKrakBujaka-O3-1g", "O3", 120)
    month_bar_plot("MpKrakAlKras-CO-1g", "CO", 5)
