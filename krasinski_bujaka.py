import os
from gios import GiosData
from select_closest_sensor import SensorLocator
from analysis import save_heatmap
from analysis import save_pairplot
from analysis import DATA_DIR


def add_diff_column(data, pair, postfix):
    pair = list(pair)
    column_name = 'PM2.5 diff({}-{})'.format(*pair)
    data[column_name] = data[pair[0] + postfix] - data[pair[1] + postfix]


def main():

    gios = GiosData()
    data = gios.get_krk_data()

    locator = SensorLocator()
    station_sensor_map = dict()
    for station in ["MpKrakAlKras", "MpKrakBujaka"]:
        station_sensor_map[station] = locator.select_for_station(station)[0]

    columns = ["UTC"]
    columns += [s + "-PM2.5-1g" for s in station_sensor_map]
    data = data[columns]
    data = data.set_index("UTC")

    for station in station_sensor_map:
        sensors_data = locator.get_data_for_station(station)
        sensor_id = station_sensor_map[station]
        cols = ["UTC", sensor_id + "_pm25"]
        sensors_data = sensors_data[cols]
        data = data.join(sensors_data.set_index("UTC"))

    save_heatmap(data, 'heatmap_krk.png')
    save_pairplot(data, 'pairplot_krk.png')

    for station, sensor in station_sensor_map.items():
        column_name = 'PM2.5 diff({}-{})'.format(station, sensor)
        data[column_name] = data[station + "-PM2.5-1g"]\
                - data[sensor + "_pm25"]

    add_diff_column(data, station_sensor_map.values(), "_pm25")
    add_diff_column(data, station_sensor_map.keys(), "-PM2.5-1g")

    describe = data.describe()
    describe.to_excel(os.path.join(DATA_DIR, "krasinski_bujaka_desc.xlsx"))
    print(describe)
    print(data.median())


if __name__ == "__main__":
    main()
